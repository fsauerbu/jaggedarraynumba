from functools import wraps
import awkward
import numba
import numpy as np

def jagged_loop(func):
    """
    Function decorator. Returns a function which accepts a JaggedArray. The
    function loops over the array and passes each row to the decorated
    function.
    """
    func = numba.njit(func)

    @numba.njit
    def main_loop(content, counts):
        cum_counts = counts.cumsum()
        length = len(counts)

        result = np.empty(length, dtype=content.dtype)

        for i in range(length):
            start = cum_counts[i] - counts[i]
            end = cum_counts[i]

            result[i] = func(content[start:end])

        return result
    
    @wraps(func)
    def wrapper(array):
        return main_loop(array.content, array.counts)
    
    return wrapper
